# GCamGOPrebuilt

GCamGo is a stripped-down version of the Pixel’s elgoog Camera app, It is commonly used by Android One devices powered by Android GO editions. It can work with OR without GApps and with MicroG etc. 



```
git clone https://gitlab.com/Imtiaz-Modder/android_packages_apps_GCamPrebuilt.git
```
Add this line in your device.mk
```
PRODUCT_PACKAGES += \
    GCamPrebuilt
```

